<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Desain_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function count_all_desain()
    {
        return $this->db->get('desain')->num_rows();
    }

    public function hargaAsc()
    {
        $this->db->select('*');
        $this->db->from('desain');
        $this->db->order_by('price', 'asc');
        return $this->db->get()->result();
    }

    public function hargaDesc()
    {
        $this->db->select('*');
        $this->db->from('desain');
        $this->db->order_by('price', 'desc');
        return $this->db->get()->result();
    }

    public function getByKategori($id)
    {
        # code...
        $this->db->select('*');
        $this->db->from('desain');
        $this->db->where('category_id', $id);
        return $this->db->get()->result();
    }

    public function getKategori()
    {
        $this->db->select('*');
        $this->db->from('product_category');
        return $this->db->get()->result();
    }

    public function get_all_desain()
    {
        return $this->db->order_by('name', 'ASC')->get('desain')->result();
    }

    public function get_all_minimalis()
    {
        return $this->db->where('category_id', 21)->order_by('name', 'ASC')->get('desain')->result();
    }

    public function get_all_klasik()
    {
        return $this->db->where('category_id', 22)->order_by('name', 'ASC')->get('desain')->result();
    }

    public function get_all_modern()
    {
        return $this->db->where('category_id', 24)->order_by('name', 'ASC')->get('desain')->result();
    }



    public function search_desain($query, $limit, $start)
    {
        $desain = $this->db->like('name', $query)->or_like('description', $query)->get('desain', $limit, $start)->result();

        return $desain;
    }

    public function count_search($query)
    {
        $count = $this->db->like('name', $query)->or_like('description', $query)->get('desain')->num_rows();

        return $count;
    }

    public function add_new_desain(array $desain)
    {
        $this->db->insert('desain', $desain);

        return $this->db->insert_id();
    }

    public function add_new_rab($id, $desain)
    {
        return $this->db->where('id', $id)->insert('desain', $desain);
    }


    public function is_desain_exist($id)
    {
        return ($this->db->where('id', $id)->get('desain')->num_rows() > 0) ? TRUE : FALSE;
    }


    public function desain_data($id)
    {
        $data = $this->db->query("
            SELECT d.*, pc.name as category_name
            FROM desain d
            JOIN product_category pc
                ON pc.id = d.category_id
            WHERE d.id = '$id'
        ")->row();

        return $data;
    }

    public function delete_desain_image($id)
    {
        return $this->db->where('id', $id)->update('desain', array('picture_name' => NULL));
    }

    public function is_desain_have_image($id)
    {
        $data = $this->desain_data($id);
        $file = $data->picture_name;

        return file_exists('./assets/uploads/desain/' . $file) ? TRUE : FALSE;
    }

    public function is_desain_have_file($id)
    {
        $data = $this->desain_data($id);
        $file = $data->file_rab;

        return file_exists('./assets/uploads/file/' . $file) ? TRUE : FALSE;
    }

    public function edit_desain($id, $desain)
    {
        return $this->db->where('id', $id)->update('desain', $desain);
    }

    public function delete_desain($id)
    {
        return $this->db->where('id', $id)->delete('desain');
    }

    public function get_all_categories()
    {
        return $this->db->order_by('name', 'ASC')->get('product_category')->result();
    }

    public function get_all_jenis()
    {
        return $this->db->order_by('name', 'ASC')->get('jenis_desain')->result();
    }

    public function get_all_rab()
    {
        return $this->db->order_by('add_date', 'ASC')->get('rab')->result();
    }

    public function category_data($id)
    {
        return $this->db->where('id', $id)->get('product_category')->row();
    }

    public function jenis_data($id)
    {
        return $this->db->where('id', $id)->get('jenis_desain')->row();
    }

    public function add_category($name)
    {
        $this->db->insert('product_category', array('name' => $name));

        return $this->db->insert_id();
    }

    public function delete_category($id)
    {
        return $this->db->where('id', $id)->delete('product_category');
    }

    public function edit_category($id, $name)
    {
        return $this->db->where('id', $id)->update('product_category', array('name' => $name));
    }


    public function get_all_coupons()
    {
        return $this->db->order_by('expired_date', 'DESC')->get('coupons')->result();
    }

    public function add_coupon(array $data)
    {
        $this->db->insert('coupons', $data);

        return $this->db->insert_id();
    }

    public function coupon_data($id)
    {
        return $this->db->where('id', $id)->get('coupons')->row();
    }

    public function edit_coupon($id, $data)
    {
        return $this->db->where('id', $id)->update('coupons', $data);
    }

    public function delete_coupon($id)
    {
        return $this->db->where('id', $id)->delete('coupons');
    }

    public function latest()
    {
        return $this->db->order_by('add_date', 'DESC')->limit(5)->get('desain')->result();
    }

    public function latest_categories()
    {
        return $this->db->order_by('id', 'DESC')->limit(5)->get('product_category')->result();
    }
}
