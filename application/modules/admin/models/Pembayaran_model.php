<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function count_all_pembayaran()
    {
        return $this->db->get('pembayaran')->num_rows();
    }

    public function sum_success_payment()
    {
        return $this->db->select('SUM(total_price) as total_payment')->where('order_status', 4)->or_where('order_status', 3)->get('orders')->row()->total_payment;
    }

    public function payment_overview()
    {
        $data = $this->db->query("
            SELECT p.*, o.order_number, c.name, c.profile_picture, o.user_id
            FROM payments p
            JOIN orders o
	            ON o.id = p.order_id
            JOIN customers c
	            ON c.user_id = o.user_id
            WHERE p.payment_status = '1'
            LIMIT 5")->result();

        return $data;
    }

    public function set_payment_status($id, $status, $order)
    {
        $this->db->where('id', $order)->update('orders', array('order_status' => 2));

        return $this->db->where('id', $id)->update('payments', array('payment_status' => $status));
    }

    public function set_status($statusp, $data)
    {
        return $this->db->where('id', $data)->update('pembayaran', array('status' => $statusp));
    }

    public function get_all_pembayaran($limit, $start)
    {
        $payments = $this->db->query("
            SELECT pe.id, p.nomor_proyek, c.name, pe.picture_name, pe.status, pe.add_date
            FROM pembayaran pe
            JOIN proyek p
                ON p.id = pe.proyek_id
            JOIN customers c
                ON c.user_id = pe.user_id
            ORDER BY pe.add_date DESC
        ");

        return $payments->result();
    }

    public function is_pembayaran_exist($id)
    {
        return ($this->db->where('id', $id)->get('pembayaran')->num_rows() > 0) ? TRUE : FALSE;
    }

    public function payment_data($id)
    {
        $payment = $this->db->query("
            SELECT p.*, o.order_number, c.name AS customer
            FROM payments p
            JOIN orders o
                ON o.id = p.order_id
            JOIN customers c
                ON c.user_id = o.user_id
            WHERE p.id = '$id'
        ");

        return $payment->row();
    }

    public function proyek_data($id)
    {
        $data = $this->db->query("
        SELECT p.id, p.nomor_proyek, c.name, c.phone_number, c.address, d.name as namedesain, d.price, r.file_rab, p.order_status, p.order_date, p.payment_method
        FROM proyek p
        JOIN customers c
            ON c.user_id = p.user_id
        JOIN rab r
            ON r.id = p.rab_id
        JOIN desain d
            ON d.id = p.desain_id
            WHERE p.id = '$id'
        ");

        
        
        return $data->row();
    }


    public function pembayaran_data($id)
    {
        $data = $this->db->query("
            SELECT pe.id, p.nomor_proyek, c.name, pe.picture_name, pe.status, pe.add_date, c.phone_number, c.address
            FROM pembayaran pe
            JOIN proyek p
                ON p.id = pe.proyek_id
            JOIN customers c
                ON c.user_id = pe.user_id
                WHERE pe.id ='$id'
        ");

        return $data->row();
    }

    public function payment_by($id)
    {
        $payments = $this->db->query("
            SELECT p.id, p.payment_date, p.order_id, p.payment_price, p.payment_status as status, o.order_number, c.name AS customer, p.payment_status
            FROM payments p
            JOIN orders o
                ON o.id = p.order_id
            JOIN customers c
                ON c.user_id = o.user_id
            WHERE o.user_id = '$id'
        ");

        return $payments->result();
    }
}