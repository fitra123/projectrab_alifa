<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Tambah Proyek</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin/products'); ?>">Desain</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <?php echo form_open_multipart('admin/orders/add_proyek'); ?>

      <div class="row">
        <div class="col-md-8">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data Proyek</h3>
                <?php if ($flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px">
                  <?php echo $flash; ?>
                </span>
                <?php endif; ?>
              </div>
        
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="pakcage">Pelanggan:</label>
                      <select name="user_id" class="form-control" id="package">
                        <option>Pilih Pelanggan</option>
                        <?php if ( count($user) > 0) : ?>
                          <?php foreach ($user as $user) : ?>
                            <option value="<?php echo $user->user_id; ?>"<?php echo set_select('user_id', $user->user_id); ?>>› <?php echo $user->name; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                      <?php echo form_error('user_id'); ?>
                  </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="pakcage">Desain:</label>
                      <select name="desain_id" class="form-control" id="package">
                        <option>Pilih Desain</option>
                        <?php if ( count($desain) > 0) : ?>
                          <?php foreach ($desain as $desain) : ?>
                            <option value="<?php echo $desain->id; ?>"<?php echo set_select('desain_id', $desain->id); ?>>› <?php echo $desain->name; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                      <?php echo form_error('desain_id'); ?>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="pakcage">RAB:</label>
                      <select name="rab_id" class="form-control" id="package">
                        <option>Pilih RAB</option>
                        <?php if ( count($rab) > 0) : ?>
                          <?php foreach ($rab as $rab) : ?>
                            <option value="<?php echo $rab->id; ?>"<?php echo set_select('rab_id', $rab->id); ?>>› <?php echo $rab->file_rab; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                      <?php echo form_error('rab_id'); ?>
                  </div>
                  </div>
                </div>

                <div class="col-md-12">
                    <div class="cart-detail p-3 p-md-4">
                        <h3 class="billing-heading mb-4">Metode Pembayaran</h3>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                          <div class="radio">
                                             <label><input type="radio" name="payment_method" class="mr-2" value="1"> Transfer bank</label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                          <div class="radio">
                                             <label><input type="radio" name="payment_method" class="mr-2" value="2" checked> Bayar ditempat</label>
                                          </div>
                                      </div>
                                  </div>
                              </div>
              
              </div>
              
            </div>
            
          </div>

        </div>
        
                <div class="card-footer text-right">
                    <input type="submit" value="Tambah Proyek Baru" class="btn btn-primary">
                </div>
            </div>
        </div>
      </div>

    </form>