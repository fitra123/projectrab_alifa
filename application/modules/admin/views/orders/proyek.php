<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kelola Daftar Proyek</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="<?php echo site_url('admin/orders/add_new_proyek'); ?>" class="btn btn-sm btn-neutral">Tambah</a>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Proyek</li>
                </ol>
              </nav>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h3 class="mb-0">Kelola Proyek</h3>
            </div>

            <?php if ( count($proyek) > 0) : ?>
            <div class="card-body p-0">
                <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Desain</th>
                    <th scope="col">File RAB</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tanggal Order</th>
                    <th scope="col">Total RAB</th>\
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($proyek as $proyek) : ?>
                  <tr>
                    <th scope="col">
                      <?php echo anchor('admin/orders/lihat/'. $proyek->id, '#'. $proyek->nomor_proyek); ?>
                    </th>
                    <td><?php echo $proyek->name; ?></td>
                    <td><?php echo $proyek->namedesain; ?></td>
                    <td>
                    <?php echo $proyek->file_rab; ?>
                    </td>
                    <td><?php echo $proyek->order_status; ?></td>
                    <td><?php echo get_formatted_date($proyek->order_date); ?>
                    </td>
                    <td>
                    Rp <?php echo format_rupiah($proyek->price); ?>
                    </td>
                    <td>
                   
              <a href="<?php echo site_url('admin/desain/edit/'. $proyek->id); ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
              <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
                </div>
            
            <div class="card-footer">
                <?php echo $pagination; ?>
            </div>
            <?php else : ?>
             <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="alert alert-primary">
                            Belum ada data proyek yang ditambahkan. Silahkan menambahkan baru.
                        </div>
                    </div>
                    <div class="col-md-4">
                        <a href="<?php echo site_url('admin/orders/add_new_proyek'); ?>"><i class="fa fa-plus"></i> Tambah proyek baru</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
          </div>
        </div>
      </div>