<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kelola Pembayaran</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h3 class="mb-0">Kelola Pembayaran</h3>
            </div>

            <?php if ( count($pembayaran) > 0) : ?>
            <div class="card-body p-0">
                <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Pembayaran Order</th>
                    <th scope="col">Customer</th>
                   
                    <th scope="col">Status</th>
                    <th scope="col">Tanggal Ditambah</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($pembayaran as $pembayaran) : ?>
                  <tr>
                   
                    <td>#<?php echo anchor('admin/pembayaran/lihat/'. $pembayaran->id, $pembayaran->nomor_proyek); ?></td>
                    <td>
                      <?php echo $pembayaran->name; ?>
                    </td>
                   
                    <td>
                     <?php echo $pembayaran->status; ?>
                    </td>
                    <td>
                       <?php echo $pembayaran->add_date; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
                </div>
            
            <div class="card-footer">
                <?php echo $pagination; ?>
            </div>
            <?php else : ?>
             <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="alert alert-primary">
                            Belum ada data pembayaran yang ditambahkan.
                        </div>
                    </div>
                    
                </div>
            </div>
            <?php endif; ?>
            
          </div>
        </div>
      </div>