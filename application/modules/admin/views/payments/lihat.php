<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Pembayaran #<?php echo $data->nomor_proyek; ?></h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin/orders'); ?>">Order</a></li>
                  <li class="breadcrumb-item active" aria-current="page">#<?php echo $data->nomor_proyek; ?></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">

      <div class="row">
        <div class="col-md-8">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data Pembayaran</h3>
                <?php if ($pembayaran_flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px;"><?php echo $pembayaran_flash; ?></span>
                <?php endif; ?>
              </div>
        
              <div class="card-body p-0">
                <table class="table align-items-center table-flush table-striped">
                    <tr>
                        <td>Nomor</td>
                        <td><b>#<?php echo $data->nomor_proyek; ?></b></td>
                    </tr>
                    <tr>
                        <td>Nama Customer</td>
                        <td><b><?php echo $data->name; ?></b></td>
                    </tr>
                    <tr>
                        <td>Gambar</td>
                        <td><b><img width = 250px height = 250px src="<?php echo base_url('assets/uploads/bukti/'. $data->picture_name );  ?> "></b></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><b><?php echo $data->status; ?></b></td>
                    </tr>
                    <tr>
                        <td>Tanggal Ditambah</td>
                        <td><b><?php echo $data->add_date; ?></b></td>
                    </tr>
                </table>
              </div>
              <form action="<?php echo site_url('admin/pembayaran/statuspembayaran'); ?>" method="POST">
                <input type="hidden" name="order" value="<?php echo $data->id; ?>">
                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                
                        <select class="form-control" id="statusp" name="statusp">
                        <option value="Menunggu Konfirmasi"<?php echo ($data->status == "Menunggu Konfirmasi") ? ' selected' : ''; ?>>Menunggu Konfirmasi</option>
                          <option value="Dikonfirmasi"<?php echo ($data->status == "Dikonfirmasi") ? ' selected' : ''; ?>>Dikonfirmasi</option>
                          <option value="Belum Ada Pembayaran"<?php echo ($data->status == "Belum Ada Pembayaran") ? ' selected' : ''; ?>>Belum Ada Pembayaran</option>
                          
                        </select>
                        </div>
                        </div>
                        
                    <div class="col-md-2">
                      <div class="text-right">
                        <input type="submit" value="OK" class="btn btn-md btn-primary">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
      
            
          </div>

        </div>
        <div class="col-md-4">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="mb-0">Data Penerima</h3>
                </div>
                <div class="card-body p-0">
                    <table class="table align-items-center table-flush table-hover">
                        <tr>
                            <td>Nama</td>
                            <td><b><?php echo $data->name; ?></b></td>
                        </tr>
                        <tr>
                            <td>No. HP</td>
                            <td><b><?php echo $data->phone_number; ?></b></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td><div style="white-space: initial;"><b><?php echo $data->address; ?></b></div></td>
                        </tr>
                    </table>
                </div>
            </div>

                </div>
      </div>