<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Tambah Data RAB</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin/products'); ?>">Desain</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <?php echo form_open_multipart('admin/desain/add_rab2'); ?>

      <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-md-8">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data RAB</h3>
                <?php if ($flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px">
                  <?php echo $flash; ?>
                </span>
                <?php endif; ?>
              </div>
        
              <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                  <h3 class="mb-0">Data RAB</h3>
              </div>
              <div class="card-body p-0">
              <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">File RAB</th>
                    <th scope="col">Tanggal Tambah</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($rab as $rab) : ?>
                  <tr>
                    <td>
                      <?php echo $rab->name; ?>
                    </td>
                    <td>
                      <?php echo $rab->file_rab; ?>
                    </td>
                    <td>
                      <?php echo $rab->add_date; ?>
                    </td>
                    <td>
                      <a href="<?php echo base_url(); ?>./assets/uploads/rab/<?php echo $rab->file_rab; ?>">Download</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
              </div>
            </div>
        </div>
      </div>
      </div>
      </div>
      <div class="col-md-4">
            <div class="card card-primary">
              <div class="card-header">
                    <h3 class="mb-0">File</h3>
                </div>
                <div class="card-body">
                   <div class="form-group">
                   <label class="form-control-label" for="name">Nama Desain:</label>
                   <input type="hidden" name="desain_id" value="<?= $_desain_id; ?>" />
                  <input type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control" id="name">
                  <?php echo form_error('name'); ?>
                     <label class="form-control-label" for="pic">RAB:</label>
                     <input type="file" name="filerab" class="form-control" id="fil">
                     <small class="text-muted">Pilih file pdf</small>
                   </div>
                </div>
                <div class="card-footer text-right">
                    <input type="submit" value="Tambah Data Baru" class="btn btn-primary">
                </div>
            </div>
        </div>
      </div>

    </form>

    