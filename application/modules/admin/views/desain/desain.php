<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Kelola Desain</h6> -->
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="<?php echo site_url('admin/desain/add_new_desain'); ?>" class="btn btn-sm btn-neutral">Tambah</a>

        </div>
        <form action="<?php echo base_url('admin/desain/getByKategori'); ?>" method="post">
          <table>
            <tr>
              <td>
                <select name="kategori" id="kategori" class="form-control">
                  <option value="">Pilih Berdasarkan Kategori</option>
                  <?php foreach ($getKategori as $row) { ?>
                    <option value="<?php echo $row->id; ?>"> <?php echo $row->name; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <button type="submit" class="btn btn-md btn-info">Search..</button>
              </td>
            </tr>
          </table>
        </form>
        <form action="<?php echo base_url('admin/desain/sortingByAsc'); ?>" method="post">
          <button type="submit" class="btn btn-md btn-warning">Urutkan dari harga terendah [A-Z]</button>
        </form>
        <form action="<?php echo base_url('admin/desain/sortingByDesc'); ?>" method="post">
          <button type="submit" class="btn btn-md btn-danger">Urutkan dari harga tertinggi [Z-A]</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header">
          <div class="col-lg-6">
            <h3 class="mb-0">Kelola Desain</h3>
          </div>
          <div class="col-lg-6">

          </div>

        </div>

        <?php if (count($desain) > 0) : ?>
          <div class="card-body">
            <div class="row">
              <?php foreach ($desain as $desain) : ?>
                <div class="col-md-3">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-heading"><?php echo $desain->name; ?></h3>
                    </div>
                    <div class="card-body">
                      <div class="text-center">
                        <img alt="<?php echo $desain->name; ?>" class="img img-fluid rounded" src="<?php echo base_url('assets/uploads/desain/' . $desain->picture_name); ?>" style="width: 1000px; max-height: 800px">
                        <br>
                        <br>
                        Rp <?php echo format_rupiah($desain->price); ?>
                      </div>

                    </div>
                    <div class="card-footer text-center">
                      <a href="<?php echo site_url('admin/desain/lihat/' . $desain->id); ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                      <a href="<?php echo site_url('admin/desain/edit/' . $desain->id); ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="card-footer">
            <?php echo $pagination; ?>
          </div>
        <?php else : ?>
          <div class="card-body">
            <div class="row">
              <div class="col-md-8">
                <div class="alert alert-primary">
                  Belum ada data desain yang ditambahkan. Silahkan menambahkan baru.
                </div>
              </div>
              <div class="col-md-4">
                <a href="<?php echo site_url('admin/desain/add_new_desain'); ?>"><i class="fa fa-plus"></i> Tambah desain baru</a>
                <br>
                <a href="<?php echo site_url('admin/products/category'); ?>"><i class="fa fa-list"></i> Kelola kategori</a>
              </div>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>
  </div>