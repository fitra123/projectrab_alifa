<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        verify_session('admin');

        $this->load->model(array(
            'order_model' => 'order',
            'proyek_model' => 'proyek',
            'desain_model' => 'desain',
            'rab_model' => 'rab',
            'user_model' => 'user'
        ));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $params['title'] = 'Kelola Order';

        $config['base_url'] = site_url('admin/orders/index');
        $config['total_rows'] = $this->order->count_all_orders();
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);
 
        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
 
        $orders['orders'] = $this->order->get_all_orders($config['per_page'], $page);
        $proyek['proyek'] = $this->proyek->get_all_proyek($config['per_page'], $page);
        $orders['pagination'] = $this->pagination->create_links();

        $this->load->view('header', $params);
        $this->load->view('orders/orders', $orders);
        $this->load->view('footer');
    }

    public function view($id = 0)
    {
        if ( $this->order->is_order_exist($id))
        {
            $data = $this->order->order_data($id);
            $items = $this->order->order_items($id);
            $banks = json_decode(get_settings('payment_banks'));
            $banks = (Array) $banks;
 
            $params['title'] = 'Order #'. $data->order_number;

            $order['data'] = $data;
            $order['items'] = $items;
            $order['delivery_data'] = json_decode($data->delivery_data);
            $order['banks'] = $banks;
            $order['order_flash'] = $this->session->flashdata('order_flash');
            $order['payment_flash'] = $this->session->flashdata('payment_flash');

            $this->load->view('header', $params);
            $this->load->view('orders/view', $order);
            $this->load->view('footer');
        }
        else
        {
            show_404();
        }
    }

    public function lihat($id = 0)
    {
        if ( $this->proyek->is_proyek_exist($id))
        {
            $data = $this->proyek->proyek_data($id);
            $params['title'] = 'Proyek #'. $data->nomor_proyek;

            $proyek['data'] = $data;

            
            $proyek['proyek_flash'] = $this->session->flashdata('proyek_flash');
            $this->load->view('header', $params);
            $this->load->view('orders/lihat', $proyek);
            $this->load->view('footer');
        }
        else
        {
            show_404();
        }
    }

    public function status()
    {
        $status = $this->input->post('status');
        $order = $this->input->post('order');

        $this->order->set_status($status, $order);
        $this->session->set_flashdata('order_flash', 'Status berhasil diperbarui');

        redirect('admin/orders/view/'. $order);
    }

    public function statusproyek()
    {
        $status = $this->input->post('status');
        $order = $this->input->post('order');

        $this->proyek->set_status($status, $order);
        $this->session->set_flashdata('proyek_flash', 'Status berhasil diperbarui');

        redirect('admin/orders/lihat/'. $order);
    }

    public function add_new_proyek()
    {
        $params['title'] = 'Tambah Proyek Baru';

        $proyek['flash'] = $this->session->flashdata('add_new_proyek_flash');
        $proyek['user'] = $this->user->get_all_user();
        $proyek['desain'] = $this->desain->get_all_desain();
        $proyek['rab'] = $this->rab->get_all_rab();

        $this->load->view('header', $params);
        $this->load->view('orders/add_new_proyek', $proyek);
        $this->load->view('footer');
    }

    public function add_proyek()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('user_id', 'Nama User', 'trim|required');
        $this->form_validation->set_rules('desain_id', 'Desain', 'trim|required');
        $this->form_validation->set_rules('rab', 'RAB', 'max_length[512]');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->add_new_proyek();
        }
        else
        {

            
            $user_id = $this->input->post('user_id');
            $desain_id = $this->input->post('desain_id');
            $rab_id = $this->input->post('rab_id');
            $payment_method= $this->input->post('payment_method');
            $date = date('Y-m-d H:i:s');

            $nomor_proyek = $this->_create_order_number($user_id, $desain_id, $rab_id);
            

            // $category_data = $this->desain->category_data($category_id);
            // $category_name = $category_data->name;
            // $category_data = $this->desain->category_data($category_id);
            // $category_name = $category_data->name;
            $user_data = $this->proyek->user_data($user_id);
            $user_name = $user_data->name;
            $desain_data = $this->proyek->desain_data($desain_id);
            $desain_name = $desain_data->name;
            $rab_data = $this->proyek->rab_data($rab_id);
            $rab_name = $rab_data->name;

            

            
            $proyek['nomor_proyek'] = $nomor_proyek;
            $proyek['user_id'] = $user_id;
            $proyek['desain_id'] = $desain_id;
            $proyek['rab_id'] = $rab_id;
            $proyek['order_status'] = "Dalam Proses bayar";
            $proyek['order_date'] = $date;
            $proyek['payment_method'] = $payment_method;
            

            $this->proyek->add_new_proyek($proyek);
            $this->session->set_flashdata('add_new_proyek_flash', 'Proyek baru berhasil ditambahkan!');

            redirect('admin/orders/add_new_proyek');
        }
    }

    public function _create_order_number($user_id, $desain_id, $rab_id)
    {
        $this->load->helper('string');

        $alpha = strtoupper(random_string('alpha', 3));
        $num = random_string('numeric', 3);
        


        $number = $alpha . date('j') . date('n') . date('y') . $user_id . $desain_id . $rab_id . $num;
        //Random 3 letter . Date . Month . Year . User ID . Desain Id . Rab Id .  Numeric

        return $number;
    }
}