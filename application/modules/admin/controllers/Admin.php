<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        verify_session('admin');

        $this->load->model(array(
            'desain_model' => 'desain',
            'customer_model' => 'customer',
            'proyek_model' => 'proyek',
            'payment_model' => 'payment'
        ));
    }

    public function index()
    {
        $params['title'] = 'Admin '. get_store_name();

        $overview['total_desain'] = $this->desain->count_all_desain();
        $overview['total_customers'] = $this->customer->count_all_customers();
        $overview['total_proyek'] = $this->proyek->count_all_proyek();
       

        $overview['desain'] = $this->desain->latest();
        $overview['proyek'] = $this->proyek->latest_proyek();
        $overview['customers'] = $this->customer->latest_customers();

        // $overview['order_overviews'] = $this->order->order_overview();
        // $overview['income_overviews'] = $this->order->income_overview();

        $this->load->view('header', $params);
        $this->load->view('overview', $overview);
        $this->load->view('footer');
    }
}