<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Desain extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        verify_session('admin');

        $this->load->model(array(
            'product_model' => 'product',
            'order_model' => 'order',
            'desain_model' => 'desain',
            'rab_model' => 'rab',
            'gambar_model' => 'gambar',
            'proyek_model' => 'proyek'
        ));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $params['title'] = 'Kelola Desain ' . get_store_name();

        $config['base_url'] = site_url('admin/desain/index');
        $config['total_rows'] = $this->desain->count_all_desain();
        $config['per_page'] = 16;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);

        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $desain['desain'] = $this->desain->get_all_desain($config['per_page'], $page);
        $desain['pagination'] = $this->pagination->create_links();
        $desain['getKategori'] = $this->desain->getKategori();

        // $kategoriId = $this->input->post('kategori');

        // $desain['getDataByKategori'] = $this->desain->getByKategori($kategoriId);

        $this->load->view('header', $params);
        $this->load->view('desain/desain', $desain);
        $this->load->view('footer');
    }

    public function getByKategori()
    {
        $params['title'] = 'Kelola Desain ' . get_store_name();

        $config['base_url'] = site_url('admin/desain/index');
        $config['total_rows'] = $this->desain->count_all_desain();
        $config['per_page'] = 16;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);

        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $kategoriId = $this->input->post('kategori');
        // $desain['desain'] = $this->desain->get_all_desain($config['per_page'], $page);
        $desain['pagination'] = $this->pagination->create_links();
        $desain['getKategori'] = $this->desain->getKategori();


        $desain['desain'] = $this->desain->getByKategori($kategoriId);

        $this->load->view('header', $params);
        $this->load->view('desain/desain', $desain);
        $this->load->view('footer');
    }

    public function sortingByAsc()
    {
        $params['title'] = 'Kelola Desain ' . get_store_name();

        $config['base_url'] = site_url('admin/desain/index');
        $config['total_rows'] = $this->desain->count_all_desain();
        $config['per_page'] = 16;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);

        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $kategoriId = $this->input->post('kategori');
        // $desain['desain'] = $this->desain->get_all_desain($config['per_page'], $page);
        $desain['pagination'] = $this->pagination->create_links();
        $desain['getKategori'] = $this->desain->getKategori();


        $desain['desain'] = $this->desain->hargaAsc($config['per_page'], $page);

        $this->load->view('header', $params);
        $this->load->view('desain/desain', $desain);
        $this->load->view('footer');
    }

    public function sortingByDesc()
    {
        $params['title'] = 'Kelola Desain ' . get_store_name();

        $config['base_url'] = site_url('admin/desain/index');
        $config['total_rows'] = $this->desain->count_all_desain();
        $config['per_page'] = 16;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);

        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $kategoriId = $this->input->post('kategori');
        // $desain['desain'] = $this->desain->get_all_desain($config['per_page'], $page);
        $desain['pagination'] = $this->pagination->create_links();
        $desain['getKategori'] = $this->desain->getKategori();


        $desain['desain'] = $this->desain->hargaDesc($config['per_page'], $page);

        $this->load->view('header', $params);
        $this->load->view('desain/desain', $desain);
        $this->load->view('footer');
    }

    public function search()
    {
        $query = $this->input->get('search_query');
        $query = html_escape($query);

        $params['title'] = 'Cari "' . $query . '"';
        $params['query'] = $query;

        $config['base_url'] = site_url('admin/desain/search');
        $config['total_rows'] = $this->product->count_all_products();
        $config['per_page'] = 16;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);

        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['reuse_query_string'] = TRUE;
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $products['products'] = $this->product->search_products($query, $config['per_page'], $page);
        $products['pagination'] = $this->pagination->create_links();
        $products['count'] = $this->product->count_search($query);

        $this->load->view('header', $params);
        $this->load->view('products/search', $products);
        $this->load->view('footer');
    }

    public function add_new_product()
    {
        $params['title'] = 'Tambah Desain Baru';

        $product['flash'] = $this->session->flashdata('add_new_product_flash');
        $product['categories'] = $this->product->get_all_categories();

        $this->load->view('header', $params);
        $this->load->view('desain/add_new_product', $product);
        $this->load->view('footer');
    }

    public function add_new_proyek()
    {
        $params['title'] = 'Tambah proyek Baru';

        $proyeks['flash'] = $this->session->flashdata('add_new_proyek_flash');
        $proyeks['proyek'] = $this->proyek->get_all_proyek();

        $this->load->view('header', $params);
        $this->load->view('desain/add_new_proyek', $proyeks);
        $this->load->view('footer');
    }

    public function add_new_desain()
    {
        $params['title'] = 'Tambah Desain Baru';

        $product['flash'] = $this->session->flashdata('add_new_product_flash');
        $product['categories'] = $this->product->get_all_categories();
        $product['jenis'] = $this->product->get_all_jenis();

        $this->load->view('header', $params);
        $this->load->view('desain/add_new_desain', $product);
        $this->load->view('footer');
    }



    public function add_product()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('name', 'Nama produk', 'trim|required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('price', 'Harga produk', 'trim|required');
        $this->form_validation->set_rules('stock', 'Stok barang', 'required|numeric');
        $this->form_validation->set_rules('unit', 'Satuan barang', 'required');
        $this->form_validation->set_rules('description', 'Deskripsi produk', 'max_length[512]');

        if ($this->form_validation->run() == FALSE) {
            $this->add_new_product();
        } else {
            $name = $this->input->post('name');
            $category_id = $this->input->post('category_id');
            $price = $this->input->post('price');
            $stock = $this->input->post('stock');
            $unit = $this->input->post('unit');
            $desc = $this->input->post('desc');
            $date = date('Y-m-d H:i:s');

            $config['upload_path'] = './assets/uploads/products/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if (isset($_FILES['picture']) && @$_FILES['picture']['error'] == '0') {
                if (!$this->upload->do_upload('picture')) {
                    $error = array('error' => $this->upload->display_errors());

                    show_error($error);
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                }
            }

            $category_data = $this->product->category_data($category_id);
            $category_name = $category_data->name;

            $sku = create_product_sku($name, $category_name, $price, $stock);

            $product['category_id'] = $category_id;
            $product['sku'] = $sku;
            $product['name'] = $name;
            $product['description'] = $desc;
            $product['price'] = $price;
            $product['stock'] = $stock;
            $product['product_unit'] = $unit;
            $product['picture_name'] = $file_name;
            $product['add_date'] = $date;

            $this->product->add_new_product($product);
            $this->session->set_flashdata('add_new_product_flash', 'Desain baru berhasil ditambahkan!');

            redirect('admin/desain/add_new_product');
        }
    }

    public function add_desain()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('name', 'Nama Desain', 'trim|required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('price', 'Total RAB', 'trim|required');
        $this->form_validation->set_rules('description', 'Deskripsi produk', 'max_length[512]');

        if ($this->form_validation->run() == FALSE) {
            $this->add_new_desain();
        } else {
            $name = $this->input->post('name');
            $category_id = $this->input->post('category_id');
            $jenis = $this->input->post('jenis_desain');
            $price = $this->input->post('price');
            $desc = $this->input->post('description');
            $date = date('Y-m-d H:i:s');

            $config['upload_path'] = './assets/uploads/desain/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if (isset($_FILES['picture']) && @$_FILES['picture']['error'] == '0') {
                if (!$this->upload->do_upload('picture')) {
                    $error = array('error' => $this->upload->display_errors());

                    show_error($error);
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                }
            }

            $category_data = $this->desain->category_data($category_id);
            $category_name = $category_data->name;






            $desain['category_id'] = $category_id;
            $desain['jenis_desain'] = 2;
            $desain['name'] = $name;
            $desain['description'] = $desc;
            $desain['price'] = $price;
            $desain['picture_name'] = $file_name;
            $desain['add_date'] = $date;

            $this->desain->add_new_desain($desain);
            $this->session->set_flashdata('add_new_desain_flash', 'Desain baru berhasil ditambahkan!');

            redirect('admin/desain/add_new_desain');
        }
    }
    public function add_rab($id = 0)
    {
        if ($this->desain->is_desain_exist($id)) {
            $data = $this->desain->desain_data($id);

            $params['title'] = $data->name;

            $desain['desain'] = $data;
            $desain['flash'] = $this->session->flashdata('desain_flash');
            $desain['orders'] = $this->order->desain_ordered($id);
            $desain['rab'] = $this->rab->rab($id);


            //INI KIRIM DULU COK KE PAGE NYA, (KARNA KAU PAKAI '$desain_id = $this->input->post('desain_id');' DI LINE 290)
            $desain['_desain_id'] = $id;

            $this->load->view('header', $params);
            $this->load->view('desain/add_new_rab', $desain);
            $this->load->view('footer');
        } else {
            show_404();
        }
    }
    public function add_rab2()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('name', 'Nama File', 'trim|required|min_length[4]|max_length[255]');

        if ($this->form_validation->run() == TRUE) {
            $name = $this->input->post('name');
            $desain_id = $this->input->post('desain_id');
            $date = date('Y-m-d H:i:s');

            $config['upload_path'] = './assets/uploads/rab/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if (isset($_FILES['filerab']) && @$_FILES['filerab']['error'] == '0') {
                if (!$this->upload->do_upload('filerab')) {
                    $error = array('error' => $this->upload->display_errors());

                    show_error($error);
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                }
            }

            //INI HA COK
            $rab = array(
                "desain_id" => $desain_id,
                "name" => $name,
                "file_rab" => $file_name,
                "add_date" => $date
            );
            $this->rab->add_new_rab($rab); //-->>>>>> VRIABEL RAB INI HA COK, GAK ADAK KO DEFINISIKAN !

            $this->session->set_flashdata('add_new_desain_flash', 'Desain baru berhasil ditambahkan!');

            redirect('admin/desain/add_rab/' . $desain_id); //-->>>> LARIKAN KE HALAMAN SEBELUMNYA (HALAMAN RAB DARI DESAIN ID = $desain_id)
        }
    }

    function download($id)
    {
        $data = $this->rab->download($id);
        $rab['rab'] = $data;
        force_download('./assets/uploads/rab/' . $data['file_rab'], NULL);
    }

    public function add_gambar($id = 0)
    {
        if ($this->desain->is_desain_exist($id)) {
            $data = $this->desain->desain_data($id);

            $params['title'] = $data->name;

            $desain['_desain_id'] = $id;

            $desain['desain'] = $data;
            $desain['flash'] = $this->session->flashdata('desain_flash');
            $desain['orders'] = $this->order->desain_ordered($id);
            $desain['gambar'] = $this->gambar->gambar($id);

            $this->load->view('header', $params);
            $this->load->view('desain/add_new_gambar', $desain);
            $this->load->view('footer');
        } else {
            show_404();
        }
    }

    public function add_gambar2()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('judul', 'Nama Judul', 'trim|required|min_length[4]|max_length[255]');

        if ($this->form_validation->run() == TRUE) {
            $judul = $this->input->post('judul');
            $desain_id = $this->input->post('desain_id');
            $date = date('Y-m-d H:i:s');


            $config['upload_path'] = './assets/uploads/detail/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if (isset($_FILES['picture']) && @$_FILES['picture']['error'] == '0') {
                if (!$this->upload->do_upload('picture')) {
                    $error = array('error' => $this->upload->display_errors());

                    show_error($error);
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                }
            }

            //INI HA COK
            $gambar = array(
                "desain_id" => $desain_id,
                "picture_name" => $file_name,
                "judul" => $judul,
                "add_date" => $date
            );
            $this->gambar->add_new_gambar($gambar); //-->>>>>> VRIABEL RAB INI HA COK, GAK ADAK KO DEFINISIKAN !

            $this->session->set_flashdata('add_new_desain_flash', 'Desain baru berhasil ditambahkan!');

            redirect('admin/desain/add_gambar/' . $desain_id); //-->>>> LARIKAN KE HALAMAN SEBELUMNYA (HALAMAN RAB DARI DESAIN ID = $desain_id)
        }
    }

    public function edit($id = 0)
    {
        if ($this->desain->is_desain_exist($id)) {
            $data = $this->desain->desain_data($id);

            $params['title'] = 'Edit ' . $data->name;

            $desain['flash'] = $this->session->flashdata('edit_desain_flash');
            $desain['desain'] = $data;
            $desain['categories'] = $this->desain->get_all_categories();

            $this->load->view('header', $params);
            $this->load->view('desain/edit_desain', $desain);
            $this->load->view('footer');
        } else {
            show_404();
        }
    }

    public function edit_desain()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('name', 'Nama Desain', 'trim|required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('price', 'Total RAB', 'trim|required');
        $this->form_validation->set_rules('description', 'Deskripsi desain', 'max_length[512]');

        if ($this->form_validation->run() == FALSE) {
            $id = $this->input->post('id');
            $this->edit($id);
        } else {
            $id = $this->input->post('id');
            $data = $this->desain->desain_data($id);
            $current_picture = $data->picture_name;

            $name = $this->input->post('name');
            $category_id = $this->input->post('category_id');
            $price = $this->input->post('price');
            $desc = $this->input->post('description');
            $date = date('Y-m-d H:i:s');

            $config['upload_path'] = './assets/uploads/desain/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if (isset($_FILES['picture']) && @$_FILES['picture']['error'] == '0') {
                if ($this->upload->do_upload('picture')) {
                    $upload_data = $this->upload->data();
                    $new_file_name = $upload_data['file_name'];

                    if ($this->desain->is_desain_have_image($id)) {
                        $file = './assets/uploads/desain/' . $current_picture;

                        $file_name = $new_file_name;
                        unlink($file);
                    } else {
                        $file_name = $new_file_name;
                    }
                } else {
                    show_error($this->upload->display_errors());
                }
            } else {
                $file_name = ($this->desain->is_desain_have_image($id)) ? $current_picture : NULL;
            }

            $desain['category_id'] = $category_id;
            $desain['name'] = $name;
            $desain['description'] = $desc;
            $desain['picture_name'] = $file_name;

            $this->desain->edit_desain($id, $desain);
            $this->session->set_flashdata('edit_desain_flash', 'Desain berhasil diperbarui!');

            redirect('admin/desain/lihat/' . $id);
        }
    }

    public function desain_api()
    {
        $action = $this->input->get('action');

        switch ($action) {
            case 'delete_image':
                $id = $this->input->post('id');
                $data = $this->desain->desain_data($id);
                $picture_name = $data->picture_name;
                $file = './assets/uploads/desain/' . $picture_name;

                if (file_exists($file) && is_readable($file) && unlink($file)) {
                    $this->desain->delete_desain_image($id);
                    $response = array('code' => 204, 'message' => 'Gambar berhasil dihapus');
                } else {
                    $response = array('code' => 200, 'message' => 'Terjadi kesalahan sata menghapus gambar');
                }
                break;
            case 'delete_desain':
                $id = $this->input->post('id');
                $data = $this->desain->desain_data($id);
                $picture = $data->picture_name;
                $file = './assets/uploads/desain/' . $picture;

                $this->desain->delete_desain($id);

                if (file_exists($file) && is_readable($file)) {
                    unlink($file);
                }

                $response = array('code' => 204);
                break;
        }

        $response = json_encode($response);
        $this->output->set_content_type('application/json')
            ->set_output($response);
    }

    public function view($id = 0)
    {
        if ($this->product->is_product_exist($id)) {
            $data = $this->product->product_data($id);

            $params['title'] = $data->name . ' | SKU ' . $data->sku;

            $product['product'] = $data;
            $product['flash'] = $this->session->flashdata('product_flash');
            $product['orders'] = $this->order->product_ordered($id);

            $this->load->view('header', $params);
            $this->load->view('desain/view', $product);
            $this->load->view('footer');
        } else {
            show_404();
        }
    }

    public function lihat($id = 0)
    {
        if ($this->desain->is_desain_exist($id)) {
            $data = $this->desain->desain_data($id);

            $params['title'] = $data->name;

            $desain['desain'] = $data;
            $desain['flash'] = $this->session->flashdata('desain_flash');
            $desain['orders'] = $this->order->desain_ordered($id);

            $this->load->view('header', $params);
            $this->load->view('desain/lihat', $desain);
            $this->load->view('footer');
        } else {
            show_404();
        }
    }

    public function category()
    {
        $params['title'] = 'Kelola Kategori Desain';

        $categories['categories'] = $this->product->get_all_categories();

        $this->load->view('header', $params);
        $this->load->view('desain/category', $categories);
        $this->load->view('footer');
    }

    public function minimalis()
    {
        $params['title'] = 'Desain Minimalis';

        $desain['desain'] = $this->desain->get_all_minimalis();

        $this->load->view('header', $params);
        $this->load->view('desain/minimalis', $desain);
        $this->load->view('footer');
    }

    public function klasik()
    {
        $params['title'] = 'Desain Klasik';

        $desain['desain'] = $this->desain->get_all_klasik();

        $this->load->view('header', $params);
        $this->load->view('desain/klasik', $desain);
        $this->load->view('footer');
    }

    public function modern()
    {
        $params['title'] = 'Desain Modern';

        $desain['desain'] = $this->desain->get_all_modern();

        $this->load->view('header', $params);
        $this->load->view('desain/modern', $desain);
        $this->load->view('footer');
    }

    public function category_api()
    {
        $action = $this->input->get('action');

        switch ($action) {
            case 'list':
                $categories['data'] = $this->product->get_all_categories();
                $response = $categories;
                break;
            case 'view_data':
                $id = $this->input->get('id');

                $data['data'] = $this->product->category_data($id);
                $response = $data;
                break;
            case 'add_category':
                $name = $this->input->post('name');

                $this->product->add_category($name);
                $categories['data'] = $this->product->get_all_categories();
                $response = $categories;
                break;
            case 'delete_category':
                $id = $this->input->post('id');

                $this->product->delete_category($id);
                $response = array('code' => 204, 'message' => 'Kategori berhasil dihapus!');
                break;
            case 'edit_category':
                $id = $this->input->post('id');
                $name = $this->input->post('name');

                $this->product->edit_category($id, $name);
                $response = array('code' => 201, 'message' => 'Kategori berhasil diperbarui');
                break;
        }

        $response = json_encode($response);
        $this->output->set_content_type('application/json')
            ->set_output($response);
    }

    public function coupons()
    {
        $params['title'] = 'Kelola Kupon Belanja';

        $this->load->view('header', $params);
        $this->load->view('desain/coupons');
        $this->load->view('footer');
    }

    public function _get_coupon_list()
    {
        $coupons = $this->product->get_all_coupons();
        $n = 0;

        foreach ($coupons as $coupon) {
            $coupons[$n]->credit = 'Rp ' . format_rupiah($coupon->credit);
            $coupons[$n]->start_date = get_formatted_date($coupon->start_date);
            $coupons[$n]->is_active = ($coupon->is_active == 1) ? ((strtotime($coupon->expired_date) < time()) ? 'Sudah kadaluarsa' : 'Masih berlaku') : 'Tidak aktif';
            $coupons[$n]->expired_date = get_formatted_date($coupon->expired_date);

            $n++;
        }

        return $coupons;
    }

    public function coupon_api()
    {
        $action = $this->input->get('action');

        switch ($action) {
            case 'coupon_list':
                $coupons['data'] = $this->_get_coupon_list();

                $response = $coupons;
                break;
            case 'view_data':
                $id = $this->input->get('id');

                $data['data'] = $this->product->coupon_data($id);
                $response = $data;
                break;
            case 'add_coupon':
                $name = $this->input->post('name');
                $code = $this->input->post('code');
                $credit = $this->input->post('credit');
                $start = $this->input->post('start_date');
                $end = $this->input->post('expired_date');

                $coupon = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'start_date' => date('Y-m-d', strtotime($start)),
                    'expired_date' => date('Y-m-d', strtotime($end))
                );

                $this->product->add_coupon($coupon);
                $coupons['data'] = $this->_get_coupon_list();

                $response = $coupons;
                break;
            case 'delete_coupon':
                $id = $this->input->post('id');

                $this->product->delete_coupon($id);
                $response = array('code' => 204, 'message' => 'Kupon berhasil dihapus!');
                break;
            case 'edit_coupon':
                $id = $this->input->post('id');
                $name = $this->input->post('name');
                $code = $this->input->post('code');
                $credit = $this->input->post('credit');
                $start = $this->input->post('start_date');
                $end = $this->input->post('expired_date');
                $active = $this->input->post('is_active');

                $coupon = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'start_date' => date('Y-m-d', strtotime($start)),
                    'expired_date' => date('Y-m-d', strtotime($end)),
                    'is_active' => $active
                );

                $this->product->edit_coupon($id, $coupon);
                $response = array('code' => 201, 'message' => 'Kupon berhasil diperbarui');
                break;
        }

        $response = json_encode($response);
        $this->output->set_content_type('application/json')
            ->set_output($response);
    }
}
