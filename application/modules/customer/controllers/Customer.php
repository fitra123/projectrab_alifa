<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        verify_session('customer');

        $this->load->model(array(
            'payment_model' => 'payment',
            'order_model' => 'order',
            'review_model' => 'review',
            'proyek_model' => 'proyek',
            'pembayaran_model' => 'pembayaran'
        ));
    }

    public function index()
    {
        $params['title'] = get_settings('store_tagline');

        $home['total_proyek'] = $this->proyek->count_all_proyek();
        $home['total_pembayaran'] = $this->pembayaran->count_all_pembayaran();
        $home['total_process_proyek'] = $this->proyek->count_process_proyek();
        $home['total_review'] = $this->review->count_all_reviews();

        $home['flash'] = $this->session->flashdata('store_flash');

        $this->load->view('header', $params);
        $this->load->view('home', $home);
        $this->load->view('footer');
    }
}