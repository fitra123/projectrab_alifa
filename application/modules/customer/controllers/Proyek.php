<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        verify_session('customer');

        $this->load->model(array(
            'proyek_model' => 'proyek',
            'pembayaran_model' => 'pembayaran'
        ));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $params['title'] = 'Kelola Proyek';

        $config['base_url'] = site_url('customer/proyek/index');
        $config['total_rows'] = $this->proyek->count_all_proyek();
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = floor($choice);
 
        $config['first_link']       = '«';
        $config['last_link']        = '»';
        $config['next_link']        = '›';
        $config['prev_link']        = '‹';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->load->library('pagination', $config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
 
        $proyeks['proyek'] = $this->proyek->get_all_proyek($config['per_page'], $page);
        $proyeks['pagination'] = $this->pagination->create_links();
        $proyeks['test_by_id'] = $this->pembayaran->getById(); // Koding fitra hapus lately
        $proyeks['getAllData'] = $this->proyek->getAllData();
        

        $this->load->view('header', $params);
        $this->load->view('orders/proyek', $proyeks);
        $this->load->view('footer');
    }

    public function view($id = 0)
    {
        if ( $this->proyek->is_proyek_exist($id))
        {
            $data = $this->proyek->proyek_data($id);
            $banks = json_decode(get_settings('payment_banks'));
            $banks = (Array) $banks;
 
            $params['title'] = 'Proyek #'. $data->nomor_proyek;

            $proyek['data'] = $data;
            $proyek['banks'] = $banks;
            $proyek['proyek_flash'] = $this->session->flashdata('proyek_flash');
            $proyek['payment_flash'] = $this->session->flashdata('payment_flash');

            $this->load->view('header', $params);
            $this->load->view('orders/lihat', $proyek);
            $this->load->view('footer');
        }
        else
        {
            show_404();
        }
    }

    public function status()
    {
        $status = $this->input->post('status');
        $order = $this->input->post('order');

        $this->order->set_status($status, $order);
        $this->session->set_flashdata('order_flash', 'Status berhasil diperbarui');

        redirect('customer/orders/view/'. $order);
    }

    public function bukti($id = 0)
    {
        if ( $this->proyek->is_proyek_exist($id))
        {
            $data = $this->proyek->proyek_data($id);

            $params['title'] = $data->name;

            $proyek['proyek_id'] = $id;
            $proyek['proyek'] = $data;
            $proyek['flash'] = $this->session->flashdata('proyek_flash');
            $proyek['gambar'] = $this->pembayaran->get_all_pembayaran($id);
            // $proyek['testID'] = $this->
			
			$proyek['getById'] = $this->pembayaran->getByProyekId($id);

            $this->load->view('header', $params);
            $this->load->view('payments/pembayarans', $proyek);
            $this->load->view('footer');
        }
        else
        {
            show_404();
        }
    }

   

    public function add_gambar2()
    {
        $this->form_validation->set_error_delimiters('<div class="form-error text-danger font-weight-bold">', '</div>');

        $this->form_validation->set_rules('judul', 'Nama Judul', 'trim|required|min_length[4]|max_length[255]');
        
        if ($this->form_validation->run() == TRUE) {
            $judul = $this->input->post('judul');
            $proyek_id = $this->input->post('proyek_id');
            $date = date('Y-m-d H:i:s');

           
            $config['upload_path'] = './assets/uploads/bukti/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);

            if ( isset($_FILES['picture']) && @$_FILES['picture']['error'] == '0')
            {
                if ( ! $this->upload->do_upload('picture'))
                {
                    $error = array('error' => $this->upload->display_errors());

                    show_error($error);
                }
                else
                {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                }
            }

			
            $gambar = array (
                'user_id' => get_current_user_id(),
                "proyek_id" => $proyek_id, 
                "picture_name" => $file_name, 
                "judul" => $judul,
                "status" => "Menunggu Konfirmasi",
				"add_date" => $date
			);
            $this->pembayaran->add_new_pembayaran($gambar); //-->>>>>> VRIABEL RAB INI HA COK, GAK ADAK KO DEFINISIKAN !

            $this->session->set_flashdata('add_new_gambar_flash', 'Desain baru berhasil ditambahkan!');

            redirect('customer/proyek/bukti/'.$proyek_id); //-->>>> LARIKAN KE HALAMAN SEBELUMNYA (HALAMAN RAB DARI DESAIN ID = $desain_id)
        }
    }
}