<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {
    public $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->user_id = get_current_user_id();
    }

    public function count_all_pembayaran()
    {
        $id = $this->user_id;

        return $this->db->join('proyek', 'proyek.id = pembayaran.proyek_id')->where('proyek.user_id', $id)->get('pembayaran')->num_rows();
    }

    public function get_all_($limit, $start)
    {
        $id = $this->user_id;
        $rab = $this->db->query("
            SELECT p.id, p.nomor_proyek, c.name, d.name as namedesain, d.price, r.file_rab, p.order_status, p.order_date, p.payment_method
            FROM proyek p
            LEFT JOIN customers c
                ON c.user_id = p.user_id
            LEFT JOIN rab r
                ON r.id = p.rab_id
            JOIN desain d
                ON d.id = p.desain_id
            WHERE p.user_id = '$id'
            ORDER BY r.add_date DESC
            LIMIT $start, $limit
        ");

        return $rab->result();
    }

    public function add_new_gambar(array $gambar)
    {
        $this->db->insert('gambar', $gambar);

        return $this->db->insert_id();
    }

    public function get_all_pembayaran($id)
    {
        $pembayaran = $this->db->query("
            SELECT pe.*, p.nomor_proyek
            FROM pembayaran pe
            JOIN proyek p
                ON p.id = pe.proyek_id
            WHERE pe.proyek_id = '$id'
            ORDER BY pe.add_date DESC
           
        ");

        return $pembayaran->result();
    }
    // public function gambar($id)
    // {
    //     $gambar = $this->db->query("
    //     SELECT g.id, d.id, g.picture_name, g.judul, g.add_date
    //     FROM gambar g
    //     JOIN desain d
    //         ON d.id = g.desain_id
    //     WHERE g.desain_id = '$id'
    //     ORDER BY g.add_date DESC
    // ");

    // return $gambar->result();
    // }

    public function pembayaran($id)
    {
        $pembayaran = $this->db->query("
        SELECT pe.*, p.nomor_proyek
            FROM pembayaran pe
            JOIN proyek p
                ON p.id = pe.proyek_id
        ORDER BY pe.add_date DESC

    ");
    return $pembayaran->result();
    }

    public function getById()
    {
        $id = get_current_user_id();
        $this->db->select("*");
        $this->db->from('proyek');
        $this->db->where('user_id',$id);
        return $this->db->get()->result();
    }

    public function getByProyekId($id)
    {
        $this->db->select('*');
        $this->db->from('proyek');
        $this->db->where('id',$id);
       return $this->db->get()->result();
    }

    public function register_payment($id, Array $data)
    {
        where('id', $id)->update('orders', array('order_status' => 2));
        $this->db->insert('payments', $data);

        return $this->db->insert_id();
    }

    public function payment_list()
    {
        $id = $this->user_id;

        $payments = $this->db->query("
            SELECT p.*, o.order_number
            FROM payments p
            JOIN orders o
	            ON o.id = p.order_id
            WHERE o.user_id = '$id'
            LIMIT 5");

        return $payments->result();
    }

    public function is_payment_exist($id)
    {
        return ($this->db->where('id', $id)->get('payments')->num_rows() > 0) ? TRUE : FALSE;
    }

    public function payment_data($id)
    {
        $data = $this->db->select('p.*, o.order_number')->join('orders o', 'o.id = p.order_id')->where('p.id', $id)->get('payments p')->row();

        return $data;
    }



    public function add_new_pembayaran(array $pembayaran)
    {
       return $this->db->insert('pembayaran', $pembayaran);

       
    }

    public function uploadGambar($proyek_id,$user_id,$picture_name,$status,$add_date)
    {
        $data['proyek_id'] = $proyek_id;
        $data['user_id'] = $user_id;
        $data['picture_name'] = $picture_name;
        $data['status'] = $status;
        $data['add_date'] = $add_date;

        return $this->db->insert('pembayaran',$data);
         

    }
}