<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek_model extends CI_Model {
    public $user_id;
    public function __construct()
    {
        parent::__construct();
        $this->user_id = get_current_user_id();
    }

    public function count_all_proyek()
    {
        $id = $this->user_id;

        return $this->db->where('user_id', $id)->get('proyek')->num_rows();
    }

    public function count_process_proyek()
    {
        $id = $this->user_id;

        return $this->db->where(array('user_id' => $id, 'order_status' => "Dalam Proses"))->get('proyek')->num_rows();
    }

    // public function get_all_proyek($limit, $start)
    // {
    //     $proyek = $this->db->get('proyek', $limit, $start)->result();

    //     return $proyek;
    // }

    public function get_all_rab($limit, $start)
    {
        $rab = $this->db->query("
            SELECT r.id, d.desain_id, r.name, r.file_rab, r.add_date
            FROM rab r
            JOIN desain d
                ON d.desain_id = r.desain_id
            ORDER BY r.add_date DESC
            LIMIT $start, $limit
        ");

        return $rab->result();
    }

    public function get_all_proyek($limit, $start)
    {
        $id = $this->user_id;
        $rab = $this->db->query("
            SELECT p.id, p.nomor_proyek, c.name, d.name as namedesain, d.price, r.file_rab, p.order_status, p.order_date, p.payment_method
            FROM proyek p
            LEFT JOIN customers c
                ON c.user_id = p.user_id
            LEFT JOIN rab r
                ON r.id = p.rab_id
            JOIN desain d
                ON d.id = p.desain_id
            WHERE p.user_id = '$id'
            ORDER BY r.add_date DESC
            LIMIT $start, $limit
        ");

        return $rab->result();
    }

    public function getAllData()
    {
        $user_id = get_current_user_id();
        $this->db->select('p.nomor_proyek,d.name');
        $this->db->from('proyek p');
        $this->db->join('customers u','u.user_id = p.user_id','left');
        $this->db->join('desain d','d.id = p.desain_id','left');
        // $this->db->join('')
        $this->db->where('u.user_id',$user_id);
        return $this->db->get()->result();
    }

    public function proyek_data($id)
    {
        $data = $this->db->query("
        SELECT p.id, p.nomor_proyek, c.name, c.phone_number, c.address, d.name as namedesain, d.price, r.file_rab, p.order_status, p.order_date, p.payment_method
        FROM proyek p
        JOIN customers c
            ON c.user_id = p.user_id
        JOIN rab r
            ON r.id = p.rab_id
        JOIN desain d
            ON d.id = p.desain_id
            WHERE p.id = '$id'
        ");

        
        
        return $data->row();
    }

    public function add_new_proyek(Array $proyek)
    {
        $this->db->insert('proyek', $proyek);

        return $this->db->insert_id();
    }

    public function latest_orders()
    {
        $orders = $this->db->query("
            SELECT o.id, o.order_number, o.order_date, o.order_status, o.payment_method, o.total_price, o.total_items, c.name AS coupon, cu.name AS customer
            FROM orders o
            LEFT JOIN coupons c
                ON c.id = o.coupon_id
            JOIN customers cu
                ON cu.user_id = o.user_id
            ORDER BY o.order_date DESC
            LIMIT 5
        ");

    return $orders->result();
    }

    public function is_order_exist($id)
    {
        return ($this->db->where('id', $id)->get('orders')->num_rows() > 0) ? TRUE : FALSE;
    }

    public function is_proyek_exist($id)
    {
        return ($this->db->where('id', $id)->get('proyek')->num_rows() > 0) ? TRUE : FALSE;
    }

    public function order_data($id)
    {
        $data = $this->db->query("
            SELECT o.*, c.name, c.code, p.id as payment_id, p.payment_price, p.payment_date, p.picture_name, p.payment_status, p.confirmed_date, p.payment_data
            FROM orders o
            LEFT JOIN coupons c
                ON c.id = o.coupon_id
            LEFT JOIN payments p
                ON p.order_id = o.id
            WHERE o.id = '$id'
        ");
        
        return $data->row();
    }

    


    public function set_status($order_status, $data)
    {
        return $this->db->where('id', $data)->update('proyek', array('order_status' => $order_status));
    }

    
    public function desain_ordered($id)
    {
        $orders = $this->db->query("
            SELECT oi.*, o.id as order_id, o.order_number, o.order_date, c.name, p.product_unit AS unit
            FROM order_item oi
            JOIN orders o
	            ON o.id = oi.order_id
            JOIN customers c
                ON c.user_id = o.user_id
            JOIN products p
	            ON p.id = oi.product_id
            WHERE oi.product_id = '1'");

        return $orders->result();
    }

    public function order_by($id)
    {
        return $this->db->where('user_id', $id)->order_by('order_date', 'DESC')->get('proyek')->result();
    }
    public function user_data($id)
    {
        return $this->db->where('user_id', $id)->get('customers')->row();
    }

    public function desain_data($id)
    {
        return $this->db->where('id', $id)->get('desain')->row();
    }

    public function rab_data($id)
    {
        return $this->db->where('id', $id)->get('rab')->row();
    }
    
}