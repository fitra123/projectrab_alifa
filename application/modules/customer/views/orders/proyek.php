<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Order Saya</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item active">Order</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card card-primary">
            <div class="card-body<?php echo ( count($proyek) > 0) ? ' p-0' : ''; ?>">
            <?php if ( count($proyek) > 0) : ?>
            <div class="card-body p-0">
                <div class="table-responsive">
              <!-- Projects table -->
              
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Desain</th>
                    <th scope="col">File RAB</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tanggal Order</th>
                    <th scope="col">Total RAB</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($proyek as $proyek) : ?>
                  <tr>
                    <th scope="col">
                      <?php echo anchor('customer/proyek/bukti/'. $proyek->id, '#'. $proyek->nomor_proyek); ?>
                    </th>
                    <td><?php echo $proyek->name; ?></td>
                    <td><?php echo $proyek->namedesain; ?></td>
                    <td>
                    <?php echo $proyek->file_rab; ?>
                    </td>
                    <td><?php echo $proyek->order_status; ?></td>
                    <td><?php echo get_formatted_date($proyek->order_date); ?>
                    </td>
                    <td>
                    Rp <?php echo format_rupiah($proyek->price); ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
                </div>
                <?php else : ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="alert alert-info">
                            Belum ada data order.
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>

            <?php if ($pagination) : ?>
            <div class="card-footer">
                <?php echo $pagination; ?> 
            </div>
            <?php endif; ?>

        </div>
    </section>

</div>