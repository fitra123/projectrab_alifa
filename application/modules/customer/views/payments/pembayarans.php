<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-5">
                    <h1>Pembayaran Saya</h1>
                </div>
               
                <div class="col-sm-5">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item active">Pembayaran</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <?php echo form_open_multipart('customer/proyek/add_gambar2'); ?>

      <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-md-8">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data Pembayaran</h3>
                <?php if ($flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px">
                  <?php echo $flash; ?>
                </span>
                <?php endif; ?>
              </div>
        
              <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                  <h3 class="mb-0">Data Pembayaran</h3>
              </div>
              <div class="card-body p-0">
              <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Gambar</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tanggal Tambah</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($gambar as $gambar) : ?>
                  <tr>
                    <td>
                    <img width = 250px height = 250px src="<?php echo base_url('assets/uploads/bukti/'. $gambar->picture_name );  ?> ">
                    </td>
                    <td>
                      <?php echo $gambar->judul; ?>
                    </td>
                    <td>
                      <?php echo $gambar->status; ?>
                    </td>
                    <td>
                      <?php echo $gambar->add_date; ?>
                    </td>
                   
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
              </div>
            </div>
        </div>
      </div>
      </div>
      </div>
      <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">File</h3>
                </div>
                <div class="card-body">
                   <div class="form-group">
                     <label class="form-control-label" for="pic">Judul:</label>
                     <input type="hidden" class="form-control" style="width:100px;" name="proyek_id" value="<?= $getById[0]->id; ?>" readonly/>
                  <input type="text" name="judul" value="<?php echo set_value('judul'); ?>" class="form-control" id="judul">
                  <?php echo form_error('judul'); ?>
                  <label class="form-control-label" for="pic">Foto:</label>
                     <input type="file" name="picture" class="form-control" id="pic">
                     <small class="text-muted">Pilih foto PNG atau JPG dengan ukuran maksimal 2MB</small>
                   </div>
                </div>
                <!-- <div class="card-body">
                   <div class="form-group">
                     <label class="form-control-label" for="pic">RAB:</label>
                     <input type="file" name="filerab" class="form-control" id="fil">
                     <small class="text-muted">Pilih file pdf</small>
                   </div>
                </div> -->
                <div class="card-footer text-right">
                    <input type="submit" value="Tambah Gambar" class="btn btn-primary">
                </div>
            </div>
        </div>
      </div>

    </form>

    