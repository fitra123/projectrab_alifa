<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Desain</h1>
                    
                </div>
                <div class="col-sm-6">
                <a href="<?php echo ('https://api.whatsapp.com/send?phone=6285273479260'); ?>" class="btn btn-danger">Konsultasikan Desain</a>
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item active">Daftar Desain</li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Daftar Desain</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="<?php echo site_url('admin/desain/add_new_desain'); ?>" class="btn btn-danger">Konsultasikan Desain</a>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h3 class="mb-0">Daftar Desain</h3>
            </div>

            <?php if ( count($desain) > 0) : ?>
            <div class="card-body">
                <div class="row">
                <?php foreach ($desain as $desain) : ?>
                    <div class="col-md-3">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-heading"><?php echo $desain->name; ?></h3>
                            </div>
                            <div class="card-body">
                                <div class="text-center">
                                    <img alt="<?php echo $desain->name; ?>" class="img img-fluid rounded" src="<?php echo base_url('assets/uploads/desain/'. $desain->picture_name); ?>" style="width: 1000px; max-height: 800px">
                                    <br>
                                    <br>
                                    Rp <?php echo format_rupiah($desain->price); ?>
                                </div>
                                
                            </div>
                            <div class="card-footer text-center">
                                <a href="<?php echo site_url('customer/desain/lihat/'. $desain->id); ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="card-footer">
                <?php echo $pagination; ?>
            </div>
            <?php else : ?>
             <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="alert alert-primary">
                            Belum ada data desain yang ditambahkan.
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
          </div>
        </div>
      </div>