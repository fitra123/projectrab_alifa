<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Desain</h1>
                    
                </div>
                <div class="col-sm-6">
                <a href="<?php echo ('https://api.whatsapp.com/send?phone=6285273479260'); ?>" class="btn btn-danger">Konsultasikan Desain</a>
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('customer'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('customer/desain'); ?>">Desain</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?php echo $desain->name; ?></li>
                </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0"><?php echo $desain->name; ?></h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('customer'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('customer/desain'); ?>">Desain</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?php echo $desain->name; ?></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- Page content -->
    <div class="container-fluid mt--8">
      <div class="row">
        <div class="col-md-10">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data Desain</h3>
                <?php if ($flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px">
                  <?php echo $flash; ?>
                </span>
                <?php endif; ?>
              </div>
              <div class="card-body p-0">
              <div>
                  <img alt="<?php echo $desain->name; ?>" class="img img-fluid rounded" src="<?php echo base_url('assets/uploads/desain/'. $desain->picture_name); ?>">
              </div>

                <table class="table table-hover table-striped">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><b><?php echo $desain->name; ?></b></td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td>:</td>
                        <td><b>Rp <?php echo format_rupiah($desain->price); ?></b></td>
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>:</td>
                        <td><b><?php echo $desain->category_name; ?></b></td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td>:</td>
                        <td><b><?php echo $desain->description; ?></b></td>
                    </tr>
                    
                </table>
              </div>
              <div class="card-footer text-right">
              <a href="<?php echo site_url('customer/desain/gambar/'. $desain->id); ?>" class="btn btn-warning">Gambar</a>
              
              </div>
              
            </div>
            
          </div>

        </div>
        <!-- <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-header">
                  <h3 class="mb-0">Proyek</h3>
              </div>
              <div class="card-body p-0">
              <div class="table-responsive">
              <!-- Projects table -->
              <!-- <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Nomor Proyek</th>
                    <th scope="col">Nama Pelanggan</th>
                    <th scope="col">total RAB</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($proyeks as $proyek) : ?>
                  <tr>
                    <th scope="col">
                      <?php echo $order->id; ?>
                    </th>
                    <td><?php echo anchor('admin/orders/view/'. $order->order_id, '#'. $order->order_number); ?></td>
                    <td>
                      <?php echo $order->name; ?>
                    </td>
                    <td><?php echo $order->order_qty; ?> <?php echo $order->unit; ?></td>
                    <td>Rp <?php echo format_rupiah($order->order_price); ?></td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
              </div>
            </div>
        </div> -->
      </div>

      <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
        <div class="modal-dialog modal-modal-dialog-centered modal-" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h6 class="modal-title" id="modal-title-default">Hapus Desain</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <form action="#" id="deleteDesainForm" method="POST">
        
            <input type="hidden" name="id" value="<?php echo $desain->id; ?>">

          <div class="modal-body">
              <p class="deleteText">Yakin ingin menghapus desain ini? Semua data yang terkait seperti data pemesanan juga akan dihapus. Tindakan ini tidak dapat dibatalkan.</p>
          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-danger btn-delete">Hapus</button>
              <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Batal</button>
          </div>
          </form>
      </div>
  </div>
</div>
</div>

<script>
    $('#deleteDesainForm').submit(function(e) {
        e.preventDefault();

        var btn = $('.btn-delete');
        var data = $(this).serialize();

        btn.html('<i class="fa fa-spin fa-spinner"></i> Menghapus...').attr('disabled', true);

        $.ajax({
            method: 'POST',
            url: '<?php echo site_url('admin/desain/desain_api?action=delete_desain'); ?>',
            data: data,
            success: function (res) {
                if (res.code == 204) {
                    setTimeout(function() {
                        btn.html('<i class="fa fa-check"></i> Terhapus!');
                        $('.deleteText').fadeOut(function() {
                            $(this).text('Desain berhasil dihapus')
                        }).fadeIn();
                    }, 2000);

                    setTimeout(function() {
                        $('.deleteText').fadeOut(function() {
                            $(this).text('Mengalihkan...')
                        }).fadeIn();
                    }, 4000);

                    setTimeout(function() {
                        window.location = '<?php echo site_url('admin/desain'); ?>';
                    }, 6000);
                }
                else {
                    console.log('Terjadi kesalahan sata menghapus produk');
                }
            }
        })
    })
</script>