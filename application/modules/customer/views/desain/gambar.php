<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- Header -->
    <div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1>Daftar Desain</h1>
                    
                </div>
                <div class="col-sm-10">
                <a href="<?php echo ('https://api.whatsapp.com/send?phone=6285273479260'); ?>" class="btn btn-danger">Konsultasikan Desain</a>
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo anchor(base_url(), 'Home'); ?></li>
                        <li class="breadcrumb-item active">Daftar Desain</li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Tambah Data Gambar</h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin'); ?>"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('admin/products'); ?>">Desain</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- Page content -->
    <div class="container-fluid mt--10">
      <?php echo form_open_multipart('admin/desain/add_gambar2'); ?>

      <div class="container-fluid mt--10">
      <div class="row">
        <div class="col-md-12">
          <div class="card-wrapper">
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0">Data Gambar</h3>
                <?php if ($flash) : ?>
                <span class="float-right text-success font-weight-bold" style="margin-top: -30px">
                  <?php echo $flash; ?>
                </span>
                <?php endif; ?>
              </div>
        
              <div class="col-md-20">
            <div class="card card-primary">
              <div class="card-header">
                  <h3 class="mb-0">Data Gambar</h3>
              </div>
              <div class="card-body p-0">
              <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Gambar</th>
                    <th scope="col">Judul</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($gambar as $gambar) : ?>
                  <tr>
                    <td>
                    <img width = 250px height = 250px src="<?php echo base_url('assets/uploads/detail/'. $gambar->picture_name );  ?> ">
                    </td>
                    <td>
                      <?php echo $gambar->judul; ?>
                    </td>
                  
                   
              </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
              </div>
            </div>
        </div>
      </div>
      </div>
      </div>
     

    </form>

    